import React, { useState } from 'react';
import { registerUser } from '../../services/user.service';

const LoginFrom = props => {

    const [username, setUsername] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [registerError, setRegisterError] = useState(false);

    const onStartClicked = () => {
        setIsLoading(true);
        let result;

        try {
            const { status } = registerUser(username);
            result = status === 200;
        } catch (error) {
            setRegisterError(error);
        } finally {
            setIsLoading(false);
            props.startedClicked(result, username);
        }
    };

    const onUsernameChange = event => setUsername(event.target.value.trim());

    return (
        <form>
            <div className="field">
                <label className="label">Username:</label>
                <div className="control">
                    <input style={{width: "200px"}} className="input" type="text" placeholder="Enter a username" onChange={onUsernameChange} />
                </div>
            </div>
            <div>
                <button className="button is-warning is-light" type="button" onClick={onStartClicked}>Start translating</button>
            </div>

            { isLoading && <div>Loading registration...</div>}
            { registerError && <div>{registerError.msg}</div>}
        </form>
    )
}

export default LoginFrom;