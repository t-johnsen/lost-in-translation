import React, { useState } from 'react';
import { removeSpecialCharacters, translateToSigns } from '../../services/translate.service';
import { checkTranslationString, saveTranslation } from '../../services/user.service';

const TranslationFrom = (props) => {

    const translateInput = React.useRef();
    const [translationString, setTranslationString] = useState('');
    const [cleanedUpString, setCleanedUpstring] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [registerError, setRegisterError] = useState(false);
    const [signs, setSigns] = useState([]);

    const onTranslateClicked = () => {
        setIsLoading(true);
        setRegisterError(false);

        try {
            checkTranslationString(translationString);
            let lettersList = translateToSigns(translationString);
            console.log(lettersList);

            let renderedSignes = lettersList.map((letter, i) => (
                letter !== ' ' ? <img key={i} src={require(`../../assets/images/${letter}.png`)} width="50" alt={letter}></img> : <span key={i}>*space*</span>
            ))
            setSigns(renderedSignes);
            const cleaned = removeSpecialCharacters(translationString);
            setCleanedUpstring(cleaned);
            saveTranslation(cleaned);
            console.log("END!");
        } catch (error) {
            setRegisterError(error);
        } finally {
            setIsLoading(false);
            translateInput.current.value = '';
            props.translateClicked(translationString);
            setTranslationString('');
        }
    }

    const onTranslationStringChange = event => setTranslationString(event.target.value.trim());

    return (
        <form className="container" style={{ display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
            <h1 className="subtitle" style={{ marginTop: "20px" }}>Enter an english word or sentance below:</h1>
            <div className="container" style={{ width: "500px" }}>
                <textarea type="text" className="textarea" ref={translateInput} onChange={onTranslationStringChange} placeholder="Enter word(s) to translate into sign language (max 40 characters)" rows="2" style={{ borderColor: "#845EC2", boxShadow: "0 0 3px #845EC2" }}></textarea>
            </div>
            <div className="container">
                <button className="button is-warning is-light" type="button" style={{ marginTop: "10px" }} onClick={onTranslateClicked}>Translate</button>
            </div>

            { isLoading && <div>Loading translation...</div>}
            { registerError && <div>{registerError.msg}</div>}

            <div className="card" style={{marginTop: "10px"}}>
                <div className="card-content" style={{ width: "900px", height: "200px" }}>
                    <p className="subtitle">
                        {signs}
                    </p>
                </div>
                <div className="container" style={{ textAlign: "center", alignItems: "center", marginBottom: "10px", marginTop: "10px"}}>
                <p>
                    {cleanedUpString}
                </p>

                </div>
            </div>
        </form>
    )
}

export default TranslationFrom;