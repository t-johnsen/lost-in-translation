import React from 'react';

const Head = () => {

    return (
        <div className="hero-head" style={{ backgroundColor: "#E7B355" }}>
            <nav className="navbar">
                <div className="container">
                    <div className="navbar-brand">
                        <div>
                            <a href="/">
                                <img src={require("../../assets/resources/Splash.svg")} alt="Splash" style={{ position: "absolute", top: "5px", left: "-20px", height: "80px" }} />
                                <img src={require("../../assets/resources/Logo.png")} alt="Logo" style={{ position: "absolute", height: "90px", top: "10px" }} />
                            </a>
                        </div>
                        <div className="navbar-item" style={{ left: "100px" }}>
                            <h1 title="subtitle">Lost in Translation</h1>
                        </div>
                    </div>
                    <div id="navbarMenuHeroA" className="navbar-menu">
                        <div className="navbar-end">
                            <a className="navbar-item" href="/">
                                Home
                                </a>
                            <a className="navbar-item " href="/profile">
                                Profile
                                </a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>

    )
}

export default Head;