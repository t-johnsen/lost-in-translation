import React from 'react';
import withUserCheck from '../../services/hoc/withUserCheck';
import TranslationFrom from '../forms/TranslationForm';
import Head from '../forms/Head';

const Main = () => {

    const handleTranslation = (translateThis) => {
        console.log(translateThis + " from parent");
    }

    return (
        <div>
            <section className="hero" >
                <Head></Head>
                <div className="hero-body" style={{ backgroundColor: "#FFC75F" }}>
                    <div className="container has-text-centered">
                        <h1 className="title">Translate</h1>

                    </div>
                </div>
            </section>
            <TranslationFrom translateClicked={handleTranslation} />
        </div>

    )
}

export default withUserCheck(Main);