import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => {
    return (
        <section className="hero is-medium">
            <div className="hero-body" style={{ backgroundColor: "#FFC75F" }}>
                <div className="container has-text-centered">
                    <h1 className="title">404 Not found</h1>
                    <Link className="button is-warning is-inverted is-outlined" to="/login">Go back home</Link>

                    <a href="/">
                        <img src={require("../../assets/resources/Splash.svg")} alt="Splash" style={{ position: "absolute", height: "150px" }} />
                        <img src={require("../../assets/resources/Logo-Hello.png")} alt="Logo" style={{ position: "absolute", height: "130px", left: "60%" }} />
                    </a>
                </div>
            </div>
        </section>
    )
}

export default NotFound;