import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import LoginFrom from '../forms/LoginForm';
import { getUser } from '../../services/user.service';
import Head from '../forms/Head';

const Login = () => {
    const history = useHistory();

    useEffect(() => {
        if (getUser()) history.push('/translate');
    }, [history]);

    const handleStartClicked = (result, username) => {
        if (result) history.push("/translate");
        console.log(username); //Use the username for anything?
    }

    return (
        <section className="hero is-fullheight" >
            <Head></Head>
            <div className="hero-body" style={{ backgroundColor: "#FFC75F" }}>
                <div className="container has-text-centered">
                    <h1 className="title">Lost in translation</h1>
                    <LoginFrom startedClicked={handleStartClicked} />

                </div>
            </div>
        </section>
    )
}

export default Login;