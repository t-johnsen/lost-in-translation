import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import withUserCheck from '../../services/hoc/withUserCheck';
import { deleteTranslations, deleteUserData, getTranslations } from '../../services/user.service';
import Head from '../forms/Head';


const Profile = () => {

    const history = useHistory();
    const [translations, setTranslations] = useState([]);
    const [trigger, setTrigger] = useState(false);
    const [registerError, setRegisterError] = useState(false);

    useEffect(() => {
        try {
            setTranslations(getTranslations());
        } catch (error) {
            setTranslations([]);
            setRegisterError(error);
        }

    }, [trigger]);

    const translationRender = translations.map((translation, i) => (
        <li className="translate-list_item" key={i}>
            <p>{translation.translateString}</p>
        </li>
    ));

    const handleLogout = () => {
        deleteUserData();
        history.replace('/login');
    }

    const handleClearCollection = () => {
        try {
            deleteTranslations();
        } catch (error) {
            setRegisterError(error);
        }
        setTrigger(!trigger);
    }

    return (
        <div>
            <section className="hero">
                <Head></Head>
                <div className="hero-body" style={{ backgroundColor: "#FFC75F" }}>
                    <div className="box has-text-centered" style={{ width: "400px", margin: "10px auto 10px auto" }}>
                        <h1 className="title" style={{ paddingBottom: "7px", borderBottom: "solid 3px #845EC2" }}>Profile page</h1>
                        <h1 className="subtitle">Your translations:</h1>
                        <div className="content">
                            <ol type="I">
                                {translationRender}
                            </ol>
                        </div>

                    </div>
                    <div className="container has-text-centered">

                        {registerError && <div>{registerError.msg}</div>}
                        
                        <button className="button is-warning is-light" type="button" onClick={handleClearCollection}>Clear translations</button>
                        <br />
                        <button className="button is-warning is-light" type="button" onClick={handleLogout} style={{ marginTop: "10px" }}>Log out</button>

                    </div>
                </div>
            </section>
        </div>
    )
}

export default withUserCheck(Profile);