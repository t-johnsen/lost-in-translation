import React from 'react';
import './App.css';
import Login from './components/containers/Login';
import Main from './components/containers/Main';
import Profile from './components/containers/Profile';
import NotFound from './components/containers/NotFound';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/login" component={Login} />
          <Route path="/translate" component={Main} />
          <Route path="/profile" component={Profile} />
          <Route path="*" component={NotFound} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
