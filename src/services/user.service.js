const Status = (msg, status) => {
    console.log(`Status code: ${status}\nMessage: ${msg}`);
    return {msg, status};
}

export const registerUser = ( username ) => {
    if(username === undefined || username.trim().length <= 2) throw Status('Username must contain more than two characters!', 400);
    localStorage.setItem('User', username);
    return Status('Success! User registered and logged in.', 200);
}

export const getUser = ( ) => {
    return localStorage.getItem('User') ? true : false; 
}

export const checkTranslationString = (translateString) =>{
    if(translateString === undefined || translateString.trim().length <= 1 || translateString.trim().length > 40) throw Status('Please enter vaild value. A word or a sentence and max 40 characters.', 400);

}

export const saveTranslation = ( translateString ) => {
    let collection = [];
    if(localStorage.getItem('TranslateCollection')){
        collection = JSON.parse(localStorage.getItem('TranslateCollection'));
        if(collection.length >= 10) collection.pop();
        collection.unshift({translateString});
    }else {
        collection = [{translateString}];
    }
    localStorage.setItem('TranslateCollection', JSON.stringify(collection));
    return Status('Success! Translation saved', 201);
}

export const getTranslations = () => {
    if(!localStorage.getItem('TranslateCollection')) throw Status('No saved translations.', 404);
    return JSON.parse(localStorage.getItem('TranslateCollection'));
}

export const deleteTranslations = () => {
    if(!localStorage.getItem('TranslateCollection')) throw Status('No saved translations.', 404);
    localStorage.removeItem('TranslateCollection');
    return Status('Translations cleared', 200);
}

export const deleteUserData = () => {
    localStorage.clear();
    return Status('Successfylly logged out', 200);
}