export const translateToSigns = (translateFrom) => {
    let letters = removeSpecialCharacters(translateFrom);
    letters = letters.toLowerCase().split('');
    return letters;
}
export const removeSpecialCharacters = (inputString) => {
    const regex = /[^a-zA-Z ]/g;
    return inputString.replace(regex, '');
}