import React from 'react';
import { Redirect } from 'react-router-dom';
import { getUser } from '../user.service';

const withUserCheck = Component => props => {

    const result = getUser();

    if(result){
        return <Component/>
    } else {
        return <Redirect to="/login" />
    }

}

export default withUserCheck;